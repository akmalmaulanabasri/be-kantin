from fastapi import APIRouter, Request, HTTPException, Depends, File, UploadFile, Form, Query, Body
from fastapi.responses import JSONResponse, FileResponse
from datetime import datetime
from typing import Optional
from helper import response_data, response_message
from db import db_connection
from models.item import AddSchema, EditSchema
import os, shutil, uuid

router = APIRouter()

@router.get("")
async def get_all_item():
    data = list(db_connection()["item"].find({}))
    if not data:
        return response_data(message="not found", code=404)
        
    return response_data(message="ok", data=data)

@router.get("/{id}")
async def get_one_item(id: str):
    try:
        data = db_connection()["item"].find_one({"_id": id})
        # data = {key: data[key] for key in ("_id", "name", "price", "img_url") if key in data}
    except:
        return response_data(message="not found", code=404)
    return response_data(message="ok", data=data)
        
    
@router.post("/add")
async def add_item(data: AddSchema):
    new_item = data.dict()
    
    item = {
        "_id": str(uuid.uuid4()),
        "name": new_item["name"],
        "price": new_item["price"],
        # "img_url": new_item["img_url"],
        "created_at": str(datetime.now()),
        "updated_at": str(datetime.now())
    }
    
    try:
        db_connection()["item"].insert_one(item)
        return response_message(message="Item added successfully.")
    except Exception:
        return response_message(message="Failed to add item.", code=400)
    
@router.delete("/delete/{id}")
async def delete_item(id: str):
    try:
        result = db_connection()["item"].delete_one({"_id": id})
        if result.deleted_count > 0:
            return response_message(message="Item deleted successfully.")
        else:
            return response_message(message="Item not found.", code=404)
    except Exception as e:
        return response_message(message="Failed to delete item.", code=400)

@router.put("/edit/{id}")
async def edit_item(id: str, data: EditSchema):
    updated_data = data.dict()
    try:
        result = db_connection()["item"].update_one(
            {"_id": id},
            {"$set": {**updated_data, "updated_at": str(datetime.now())}}
        )

        if result.modified_count > 0:
            return response_message(message="Item edited successfully.")
        else:
            return response_message(message="Item not found.", code=404)
    except Exception as e:
        return response_message(message="Failed to edit item.", code=400)

