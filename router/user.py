from fastapi import APIRouter, Request, HTTPException, Depends, File, UploadFile, Form, Query, Body, Header
from fastapi.responses import JSONResponse, FileResponse
from datetime import datetime
from typing import Optional
from helper import response_data, response_message
from db import db_connection
from models.item import AddSchema, EditSchema
import os, shutil, uuid
from fastapi import APIRouter, Depends, HTTPException
from pydantic import BaseModel
from typing import List
# from models.user import UserSchema, TopupSchema

router = APIRouter()

# def get_user_by_email(email: str):
#     for user in users_db:
#         if user.email == email:
#             return user
#     return None

# def get_user_by_auth(token: str = Header(...)):
#     for user in users_db:
#         if user.auth_token == token:
#             return user
#     raise HTTPException(status_code=401, detail="Unauthorized")

# @router.post("/register")
# async def register_user(user: User):
#     existing_user = get_user_by_email(user.email)
#     if existing_user:
#         return response_message("Email already registered", code=400)
    
#     user = {
#         "_id": str(uuid.uuid4()),
#         "name": user["name"],
#         "email": user["email"],
#         "password": user["password"],
#         "role": user["role"],
#         "created_at": str(datetime.now()),
#         "updated_at": str(datetime.now())
#     }
#     return response_message("User registered successfully")

# @router.post("/login")
# async def login(email: str, password: str):
#     user = get_user_by_email(email)
#     if user and user.password == password:
#         return response_message("Logged in successfully")
#     else:
#         return response_message("Invalid credentials", code=401)

# @router.get("/user/{id}")
# async def get_user(id: str):
#     data = list(db_connection()["user"].find_one({"id": id}))
#     if not data:
#         return response_message(message="User not found", code=404)
#     return response_data(message="ok", data=data)

# @router.get("/users")
# async def get_all_users():
#     data = list(db_connection()["user"].find({}))
#     if not data:
#         return response_data(message="not found", code=404)
        
#     return response_data(message="ok", data=data)

# @router.get("/balance")
# async def check_balance(user: User = Depends(get_user_by_auth)):
#     return {"balance": user.balance}

# @router.put("/topup/{id}")
# async def topup_balance(topup: Topup):
#     top_up = topup.dict()
    
#     return response_message("Balance topped up successfully")

# @router.delete("/delete/{user_id}")
# async def delete_user(id: str, user: User = Depends(get_user_by_auth)):
#     if user.role != "admin":
#         return response_message("Permission denied", code=403)
#     try:
#         result = db_connection()["user"].delete_one({"_id": id})
#         if result.deleted_count > 0:
#             return response_message(message="User deleted successfully.")
#         else:
#             return response_message(message="User not found.", code=404)
#     except Exception as e:
#         return response_message(message="Failed to delete user.", code=400)
