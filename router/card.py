from fastapi import APIRouter, Request, HTTPException, Depends, File, UploadFile, Form, Query, Body, Header
from fastapi.responses import JSONResponse, FileResponse
from datetime import datetime
from typing import Optional
from helper import response_data, response_message
from db import db_connection
from models.card import AddSchema, TopUpSchema
import os, shutil, uuid
from fastapi import APIRouter, Depends, HTTPException
from pydantic import BaseModel
from typing import List

router = APIRouter()

@router.post("/add")
async def add_card(add: AddSchema):
   new_card = add.dict()
   card = {
        "_id": str(uuid.uuid4()),
        "card_id": new_card["card_id"],
        "user_id": new_card["user_id"],
        "balance": 0,
        "created_at": str(datetime.now()),
        "updated_at": str(datetime.now()),
   }
   try:
       db_connection()["card"].insert_one(card)
       return response_message(message="Card Add Success.")
   except Exception:
       return response_message(message="Failed to add new card.", code=400)
   
@router.delete("/delete/{id}")
async def delete_card(id: str):
    try:
      result = db_connection()["card"].delete_one({"_id":id})
      if result.deleted_count > 0:
          return response_message(message="Card deleted successfully.")
      else:
          return response_message(message="Card not found.", code=404)
    except Exception as e:
        return response_message(message="Failed to delete card.", code=400)
    
@router.put("/top_up/{id}")
async def top_up(id: str, data: TopUpSchema):
    updated_data = data.dict()
    try:
        card_collection = db_connection()["card"]
        current_data = card_collection.find_one({"_id": id})
        
        if current_data:
            old_balance = current_data["balance"]
            amount_to_add = updated_data["amount"]
            new_balance = old_balance + amount_to_add
            
            result = card_collection.update_one(
                {"_id": id},
                {"$set": {"balance": new_balance, "updated_at": str(datetime.now())}}
            )
            
            if result.modified_count > 0:
                message = {
                    "message": "Top-up successful",
                    "old_balance": old_balance,
                    "new_balance": new_balance
                }
                return response_message(message=message)
            else:
                return response_message(message="Unable to top-up.", code=400)
        else:
            return response_message(message="Unable to top-up.", code=400)
    except Exception as e:
        return response_message(message=f"Top-up failed due to an error: {str(e)}", code=400)

