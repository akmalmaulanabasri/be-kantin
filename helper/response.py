from fastapi.responses import JSONResponse
from db import db_connection
from datetime import datetime
import uuid

def insert_log(log):
    db_connection()["log"].insert_one({"_id": str(uuid.uuid4()), "log": str(log), "created_at": str(datetime.now())})

def response_data(message: str, data: list = [], code: int = 200) -> JSONResponse:
    response = {"code": code, "message": message, "data": data}
    insert_log(response)
    return JSONResponse(content=message, status_code=code)
 
def response_message(message: str, code: int = 200) -> JSONResponse:
    response = {"code": code, "message": message}
    insert_log(response)
    return JSONResponse(content=message, status_code=code)

