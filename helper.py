from fastapi.responses import JSONResponse

def response_data(message: str, data: list = [], code: int = 200) -> JSONResponse:
    return JSONResponse(content={"code": code, "message": message, "data": data}, status_code=code)
 
def response_message(message: str, code: int = 200) -> JSONResponse:
    return JSONResponse(content={"code": code, "message": message}, status_code=code)

