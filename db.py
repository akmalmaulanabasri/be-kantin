from pymongo import MongoClient
from dotenv import dotenv_values

config = dotenv_values()

def db_connection():
    client = MongoClient(config["MONGO_URL"])
    db = client[config["MONGO_DB"]]
    return db