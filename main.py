import os
import shutil
import uuid
from datetime import datetime

from fastapi import FastAPI, File, UploadFile, HTTPException
from fastapi.responses import JSONResponse, FileResponse
from dotenv import dotenv_values
from pymongo import MongoClient
from router import item, order, user, log, auth, card
from typing import Optional

config = dotenv_values()

app = FastAPI(
    title="E Canteen",
)

@app.get("/")
async def hello():
    return {"message": "Hello World!"}
    
app.include_router(item.router, tags=['Item'], prefix='/api/v1/item')
app.include_router(order.router, tags=['Order'], prefix='/api/v1/order')
app.include_router(log.router, tags=['Log'], prefix='/api/v1/log')
app.include_router(auth.router, tags=['Auth'], prefix='/api/v1/auth')
app.include_router(user.router, tags=['User'], prefix='/api/v1/user')
app.include_router(card.router, tags=['Card'], prefix='/api/v1/card')
