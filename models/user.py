from pydantic import BaseModel, Field, validator
import uuid 
from typing import Optional
from datetime import datetime
import uuid
import secrets

class UserSchema(BaseModel):
    name: str
    email: str
    password: str
    auth_token: str(secrets.token_urlsafe(32))
    
    class Config:
        orm_mode = True
        
        schema_extra = {
            "example":{
                "name": "Cireng",
                "email": 1000,
                "password": 1000
            }
        }
        
class TopupSchema(BaseModel):
    topup: int
    
    class Config:
        orm_mode = True
        
        schema_extra = {
            "example":{
                "topup": 1000,
            }
        }