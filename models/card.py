from pydantic import BaseModel, Field, validator
import uuid 
from typing import Optional
from datetime import datetime
import uuid

class AddSchema(BaseModel):
    card_id: int
    user_id: int
    
    class Config:
        orm_mode = True
        
        schema_extra = {
            "example":{
                "card_id": "",
                "user_id": "",
            }
        }
        
class TopUpSchema(BaseModel):
    amount: int
    
    class Config:
        orm_mode = True
        schema_extra = {
            "example": {
                # "canteen_id": "",
                "amount": 10000,
            }
        }