from pydantic import BaseModel, Field, validator
import uuid 
from typing import Optional
from datetime import datetime
import uuid

class AddSchema(BaseModel):
    canteen_id: str
    name: str
    price: int
    img_url: Optional[str]
    
    class Config:
        orm_mode = True
        
        schema_extra = {
            "example":{
                "canteen_id": "",
                "name": "Cireng",
                "price": 1000,
                "img_url": "/",
            }
        }
        

class EditSchema(BaseModel):
    # canteen_id: Optional[str]
    name: Optional[str]
    price: int
    img_url: Optional[str]
    
    class Config:
        orm_mode = True
        schema_extra = {
            "example": {
                # "canteen_id": "",
                "name": "Updated Cireng",
                "price": 1200,
                "img_url": "/",
            }
        }