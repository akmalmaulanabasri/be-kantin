from pydantic import BaseModel
from datetime import datetime
from typing import List

class CanteenSchema(BaseModel):
    name: str
    class Config:
        orm_mode = True
        
        schema_extra = {
            "example":{
                "name": "Kantin Utama"
            }
        }

class OrderSchema(BaseModel):
    user_id: str
    merchant_id: str
    total_price: int
    class Config:
        orm_mode = True
        
        schema_extra = {
            "example":{
                "name": "Cireng",
                "email": 1000,
                "password": 1000,
                "role": "user",
            }
        }

class OrderItemSchema(BaseModel):
    price: int
    quantity: int
    class Config:
        orm_mode = True
        
        schema_extra = {
            "example":{
                "price": 1,
                "quantity": 1,
            }
        }